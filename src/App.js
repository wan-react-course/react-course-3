import logoxxx from './logo.svg';
import './App.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { HomePage } from './pages/HomePage/HomePage';
import { SecondPage } from './pages/SecondPage/SecondPage';
import { NotesPage } from './pages/NotesPage/NotesPage';
function Saludar() {
  return <h1>Hello</h1>;
}

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<NotesPage />}/>
        <Route path="/second" element={<SecondPage name="Juan"/>} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;

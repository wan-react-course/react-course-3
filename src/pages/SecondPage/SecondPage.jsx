import { useEffect, useRef, useState } from "react"
import { Link } from "react-router-dom"

class Persona {
  constructor(name) {
    this.name = name
  }
}

const p = new Persona('Juan');

export const SecondPage = (props) => {
  //     [value, dispatcher]
  const [counter, setCounter] = useState(1);
  
  const add = () => {
    setCounter(counter + 1);
  }

  const add1000Times = () => {
    for(let i=0; i < 1000; i++) {
      console.log('Corriendo!!')
      setCounter((current) => {
        return current + 1;
      });
    }
  }

  useEffect(() => {
    console.log('Ejecutando efecto despues de montar el component');
    return () => {
      console.log('Ejecutando efecto despues de desmontar el componente')
    }
  }, []);

  useEffect(() => {
    console.log('Este efecto se ejecuta despues de montar el componente y cada vez que couter cambia');
  }, [counter]);

  return (
    <div>
      <h1>Hola desde Second</h1>
      <Link to="/">ir a home</Link>
      <h1>{counter}</h1>
      <button onClick={add}>Agregar al contador</button>
      <button onClick={add1000Times}>Agregar 1000 veces al contador</button>
    </div>
  );
}
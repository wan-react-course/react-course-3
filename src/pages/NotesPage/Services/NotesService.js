export class NotesService {
  /**
   * Obtiene todas las notas que tenemos en el localStorage
   * @returns {[]}
   */
  getAllNotes() {
    const notes = localStorage.getItem('notes');
    if (notes) {
      return JSON.parse(notes)
    }
    return [];
  }

  /**
   * 
   * @param {{title, content}} data 
   * @returns {string}
   */
  saveNote(data) {
    // Traer la data actual en Notes
    // [] or [{...}, {...}, ...]
    const notes = this.getAllNotes();

    // Agregar el Note al array de Notes
    const note  = {
      id:  Date.now(), 
      ...data
    };
    notes.push(note);
  
    // Guardar el nuevo valor de notes en el local storage
    localStorage.setItem('notes', JSON.stringify(notes));

    return note;
  }
  /**
   * 
   * @param {{title, content, id}} data 
   */
  updateNote(data) {

  }
}
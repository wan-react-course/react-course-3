import { AddIcCallOutlined } from "@mui/icons-material";
import { Card, CardActionArea, CardContent, Container, Fab, Grid, Switch, Typography } from "@mui/material";
import { useCallback, useContext, useEffect, useReducer, useState } from "react";
import { useIsAdmin } from "../../hooks/useIsAdmin";
import { NotesContext } from "../../Services";
import { CreateNoteModal, noteFormReducer } from "./components/CreateNoteModal";

export const NotesPage = () => {
  const [notes, setNotes] = useState([]);
  const notesService = useContext(NotesContext);
  const [noteState, setNoteState] = useReducer(noteFormReducer, {
    title: '',
    content: '',
    open: false
  });

  const isAdmin = useIsAdmin();

  const getNotes = useCallback(async () => {
    const result = notesService.getAllNotes();
    setNotes(result);
  }, [notesService]);

  useEffect(() => {
    console.log('effect')
    getNotes();
  }, [getNotes]);

  const renderIsAdmin = useCallback(() => {
    if (isAdmin) {
      return (
        <Typography gutterBottom variant="h5" component="div">
          Hola Admin en Funcion
        </Typography>
      )
    }
    return undefined;
  }, [isAdmin])

  const titleHandler = useCallback((e) => {
    // setTitle(e.target.value);
    setNoteState({ type: 'title', data: e.target.value });
  }, []);

  const contentHandler = useCallback((e) => {
    // setContent(e.target.value);
    setNoteState({ type: 'content', data: e.target.value });
  }, []);

  /**
   * Guarda la modal en el local-storage utilizando notes service
   * y le pasa los valores de title y content que tenemos en el estado.
   */
  const save = useCallback(() => {
    // props.save({title, content});
    if (noteState.id) {

      return;
    }
    const note = notesService.saveNote({
      title: noteState.title,
      content: noteState.content
    });
    console.log(note);
    setNotes((current) => {
      return [note, ...current];
    });
    setNoteState({type: 'hideModal'});
  }, [noteState]);

  /**
   * Abre la modal con los campos en blanco
   * Para entender que exactamente esta pasando revisar {noteFormReducer} 
   */
  const showModal = useCallback(() => {
    setNoteState({type: 'openModal'})
  }, [noteState]);

  /**
   * Oculta la modal
   * Para entender que exactamente esta pasando revisar {noteFormReducer} 
   */
  const hideModal = useCallback(() => {
    setNoteState({type: 'hideModal'})
  }, []);

  /**
   * Abre la nota seleccionada en el modal
   * note -> {title, content, id}
   */
  const open = useCallback((note) => {
    setNoteState({type: 'openModal', data: note})
  }, []);

  return (
    <Container>
      <CreateNoteModal
        {...noteState}
        onTitleChange={titleHandler}
        onContentChange={contentHandler}
        onClose={hideModal}
        save={save}
      />
      {/* {(isAdmin) ? 'Hello': 'No Hello'}
      {renderIsAdmin()}
      {isAdmin && <Typography gutterBottom variant="h5" component="div">
        Hola Admin en JSX
      </Typography>} */}
      <Grid container={true} spacing={2}>
        {notes.map((note, i) => (
          <Grid item={true} key={`note-${i}`}>
            <Card>
              <CardActionArea sx={{ width: 345, height: 150}} onClick={() => open(note)}>
                <CardContent>
                  <Typography gutterBottom variant="h5" component="div">
                    {note.title}
                  </Typography>
                  <Typography variant="body2" color="text.secondary">
                    {note.content}
                  </Typography>
                  </CardContent>
              </CardActionArea>
            </Card>
          </Grid>
        ))}
      </Grid>
      <Fab color="primary" variant="extended" onClick={showModal}>
        <AddIcCallOutlined  />
        Agregar Nota
      </Fab>
    </Container>
  );
}
import { Button, Modal, TextField, Typography } from "@mui/material";
import { Box } from "@mui/system";
import { useCallback, useReducer, useState } from "react"
import "./CreateNoteModal.css";

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 450,
  bgcolor: 'background.paper',
  background: 'white',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

/**
 * 
 * @param {title, content, open} state 
 * @param {type, data} action 
 * @returns 
 */

export const noteFormReducer = (state, action) => {
  switch(action.type) {
    case 'title':
      return {
        ...state,
        title: action.data
      }
    case 'content':
      return {
        ...state,
        content: action.data
      }
    case 'setData':
      return {
        title: action.title,
        content: action.content
      }
    case 'reset': 
      return {
        title: '',
        content: ''
      }
    case 'hideModal': 
      return {
        title: '',
        content: '',
        open: false
      }
    case 'openModal':
      /**
       * action.data -> {title, content}
       */
      return {
        ...state,
        ...action.data,
        open: true
      }
    default:
      return state;
  }
}


/** 
 * Este componente es controlado por el Componente que lo esta rendereando
*/
export const CreateNoteModal = (props) => {
  return (
    <Modal 
      open={props.open}
      onClose={props.onClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box sx={style}>
        <Typography id="modal-modal-title" variant="h6" component="h2">
            Create Note
        </Typography>
        <TextField 
          className="input-style"
          label="Title"
          variant="outlined"
          value={props.title} 
          onChange={props.onTitleChange}
        />
        <TextField 
          className="input-style"
          label="Content"
          variant="outlined"
          value={props.content}
          onChange={props.onContentChange}
          multiline rows={5}
        />
        <br />
        <Button variant="contained" onClick={props.save}>Save</Button>
      </Box>
    </Modal>
  );
}
import React from 'react';
import { Link } from "react-router-dom";

export const HomePage = () => {
  return (
    // <div>
    //   <h1>Hola</h1>
    //   <Link to="/second">ir a second</Link>
    // </div>
    // <>
    //    <h1>Hola</h1>
    //    <Link to="/second">ir a second</Link>
    // </>
    <React.Fragment>
       <h1>Hola</h1>
       <Link to="/second">ir a second</Link>
    </React.Fragment>
  );
}
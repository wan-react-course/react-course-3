import React from 'react';
import { NotesService } from '../pages/NotesPage/Services/NotesService';
import { AuthService } from './AuthService';

const notesService = new NotesService();
const authService = new AuthService();

export const NotesContext = React.createContext(notesService);
export const AuthContext = React.createContext(authService);
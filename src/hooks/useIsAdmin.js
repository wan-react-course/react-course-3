import { useCallback, useContext, useEffect, useRef, useState } from "react";
import { AuthContext } from "../Services";
import { AuthService } from "../Services/AuthService";

/**
 * Hook custom
 * @returns 
 */
export function useIsAdmin() {
  const [isAdmin, setIsAdmin] = useState(false);
  const authService = useContext(AuthContext);

  const checkPermissions = useCallback(() => {
    const result = authService.isAdmin();
    setIsAdmin(result);
  }, [authService]);

  useEffect(() => {
    checkPermissions();
  }, [checkPermissions]);

  return isAdmin;
}

/**
 * Simulacion de mas o menos como funciona el hook useState de react
 * @param {*} initialValue 
 * @returns 
 */
function useStateMock(initialValue) {
  const val = useRef(initialValue);

  const dispatcher = useCallback((value) => {
    val.current = value;
  }, []);

  return [val.current, dispatcher];
}